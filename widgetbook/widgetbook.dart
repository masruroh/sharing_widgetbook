import 'package:flutter/material.dart';
import 'package:widgetbook/widgetbook.dart';

import 'categories/common_widget.dart';
import 'categories/feature.dart';

class MyWidgetbook extends StatelessWidget {
  const MyWidgetbook({Key? key}) : super(key: key);
  List<Device> get devices => [
        const Device(
          name: 'device 1',
          resolution: Resolution(
            nativeSize: DeviceSize(width: 300, height: 500),
            scaleFactor: 1,
          ),
          type: DeviceType.mobile,
        ),
        const Device(
          name: 'device 2',
          resolution: Resolution(
            nativeSize: DeviceSize(width: 300, height: 700),
            scaleFactor: 1,
          ),
          type: DeviceType.mobile,
        ),
        const Device(
          name: 'device 3',
          resolution: Resolution(
            nativeSize: DeviceSize(width: 500, height: 700),
            scaleFactor: 1,
          ),
          type: DeviceType.mobile,
        ),
      ];

  @override
  Widget build(BuildContext context) {
    return Widgetbook.material(
      appInfo: AppInfo(name: 'Share Widgetbook'),
      devices: devices,
      textScaleFactors: [0.5, 1, 2],
      themes: [
        WidgetbookTheme(name: 'Light', data: ThemeData.light()),
        WidgetbookTheme(name: 'Dark', data: ThemeData.dark()),
      ],
      categories: [
        categoryCommonWidget,
        categoryFeature,
      ],
    );
  }
}
