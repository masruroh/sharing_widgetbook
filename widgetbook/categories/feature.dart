import 'package:share_widgetbook/src/features/home/home_screen.dart';
import 'package:widgetbook/widgetbook.dart';

final WidgetbookCategory categoryFeature = WidgetbookCategory(
  name: 'Features',
  folders: [
    WidgetbookFolder(
      name: 'Home',
      widgets: [
        WidgetbookComponent(
          name: 'Homescreen',
          useCases: [
            WidgetbookUseCase(
              name: 'Male',
              builder: (context) {
                return const HomeScreen(type: HomeScreenType.male);
              },
            ),
            WidgetbookUseCase(
              name: 'Female',
              builder: (context) {
                return const HomeScreen(type: HomeScreenType.female);
              },
            ),
          ],
        ),
      ],
    ),
  ],
);
