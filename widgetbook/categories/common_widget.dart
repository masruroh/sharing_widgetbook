import 'package:flutter/material.dart';
import 'package:share_widgetbook/src/common_widgets/button.dart';
import 'package:widgetbook/widgetbook.dart';

final WidgetbookCategory categoryCommonWidget = WidgetbookCategory(
  name: 'Common Widgets',
  widgets: [
    WidgetbookComponent(
      name: 'Button',
      useCases: [
        WidgetbookUseCase(
          name: 'Primary',
          builder: (context) {
            String label = context.knobs.text(
              label: 'Label of button',
              initialValue: 'Submit',
            );
            final isActive = context.knobs.boolean(
              label: 'isActive',
              initialValue: true,
            );
            final isLoading = context.knobs.boolean(label: 'isLoading');

            return Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Button(
                    onPressed: isActive ? () {} : null,
                    label: label,
                    isLoading: isLoading,
                    type: ButtonType.primary,
                    size: ButtonSize.regular,
                  ),
                  const SizedBox(height: 10),
                  Button(
                    onPressed: isActive ? () {} : null,
                    label: label,
                    isLoading: isLoading,
                    type: ButtonType.primary,
                    size: ButtonSize.large,
                  ),
                ],
              ),
            );
          },
        ),
        WidgetbookUseCase(
          name: 'Secondary',
          builder: (context) {
            String label = context.knobs.text(
              label: 'Label of button',
              initialValue: 'Submit',
            );
            final isActive = context.knobs.boolean(
              label: 'isActive',
              initialValue: true,
            );
            final isLoading = context.knobs.boolean(label: 'isLoading');

            return Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Button(
                    onPressed: isActive ? () {} : null,
                    label: label,
                    isLoading: isLoading,
                    type: ButtonType.secondary,
                    size: ButtonSize.regular,
                  ),
                  const SizedBox(height: 10),
                  Button(
                    onPressed: isActive ? () {} : null,
                    label: label,
                    isLoading: isLoading,
                    type: ButtonType.secondary,
                    size: ButtonSize.large,
                  ),
                ],
              ),
            );
          },
        ),
      ],
    ),
  ],
);
