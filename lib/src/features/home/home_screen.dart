import 'package:flutter/material.dart';

enum HomeScreenType { male, female }

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key, this.type = HomeScreenType.male});
  final HomeScreenType type;

  bool get isMale => type == HomeScreenType.male;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Share Widgetbook'),
        backgroundColor: Colors.lightBlue,
      ),
      backgroundColor: isMale ? Colors.blueAccent : Colors.pinkAccent,
      body: const Center(
        child: Text(
          'Home Screen',
          style: TextStyle(fontSize: 40),
        ),
      ),
    );
  }
}
