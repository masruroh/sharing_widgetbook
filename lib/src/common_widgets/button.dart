import 'package:flutter/material.dart';

enum ButtonType { primary, secondary }

enum ButtonSize { regular, large }

class Button extends StatelessWidget {
  const Button({
    super.key,
    required this.onPressed,
    this.type = ButtonType.primary,
    this.size = ButtonSize.regular,
    this.isLoading = false,
    required this.label,
  });
  final Function()? onPressed;
  final ButtonType type;
  final ButtonSize size;
  final bool isLoading;
  final String label;

  ButtonStyle get style => ElevatedButton.styleFrom(
        foregroundColor: Colors.white,
        padding: EdgeInsets.zero,
        backgroundColor:
            type == ButtonType.primary ? Colors.blueAccent : Colors.pinkAccent,
        shadowColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        elevation: 0,
      );
  bool get isLarge => size == ButtonSize.large;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: isLoading ? null : onPressed,
      style: style,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            isLoading
                ? const SizedBox(
                    height: 12,
                    width: 12,
                    child: CircularProgressIndicator(
                      color: Colors.blueAccent,
                      strokeWidth: 2,
                    ),
                  )
                : Text(
                    label,
                    style: TextStyle(fontSize: isLarge ? 30 : 16),
                  ),
          ],
        ),
      ),
    );
  }
}
