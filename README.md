# share_widgetbook

Flutter project (Apps example to implement widgetbook).


## The preview apps

![Preview Apps](assets/apps.mov)
<video width="640" height="480" controls>
  <source src="assets/apps.mov" type="video/mov">
</video>


## Getting Started

This project is a starting point for a Flutter application.

A few resources library:

- [Lab: Widgetbook](https://pub.dev/packages/widgetbook)

step:
1. get the dependencies, run command `flutter pub get`
2. run the widgetbook
  
    macos: `flutter run -t widgetbook/main.dart -d macos`,

    chrome: `flutter run -t widgetbook/main.dart -d chrome`.
